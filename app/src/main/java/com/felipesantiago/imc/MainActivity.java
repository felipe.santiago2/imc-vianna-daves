package com.felipesantiago.imc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn = null;
    EditText nameEditText = null;
    EditText pesoEditText = null;
    EditText alturaEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        setupButton();
    }

    private void bindViews() {
        btn = findViewById(R.id.btn);
        nameEditText = findViewById(R.id.nameEditText);
        pesoEditText = findViewById(R.id.pesoEditText);
        alturaEditText = findViewById(R.id.alturaEditText);
    }

    private void setupButton() {
        btn.setOnClickListener(view -> Toast.makeText(this, makeText(), Toast.LENGTH_LONG).show());
    }

    private String makeText() {
        return "Olá " + nameEditText.getText() + "! seu IMC é de " + getImc();
    }

    private double getImc() {
        double peso = Double.parseDouble(pesoEditText.getText().toString());
        double altura = Double.parseDouble(alturaEditText.getText().toString());
        double imc = peso / (altura * altura) * 10000;
        return (double) Math.round(imc * 10d) / 10d;
    }
}